# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  deployment = "dyff-crds"
  name       = "${var.environment}-${local.deployment}"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }
}
